/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.mycompany.cardealer.ConfigHibernate;
import com.mycompany.cardealer.model.Car;
import com.mycompany.cardealer.model.CarDAO;
import com.mycompany.cardealer.model.CarDTO;

import com.mycompany.cardealer.model.ClientDAO;
import com.mycompany.cardealer.model.ClientDTO;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


/**
 *
 * @author RENT
 */
@ManagedBean(name = "sellCarController")
@ViewScoped
public class SellCarController implements Serializable{

    Map<String, String> params;
    Long carId;
    Car car;
    ClientDTO clientDTO;
    ClientDAO clientDAO = new ClientDAO();
    CarDTO carDTO;
    CarDAO carDAO = new CarDAO();
    
   
    
   /*@PostConstruct
    public void init(){
        params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        //String temp = params.get("car_id");
        carDTO = carDAO.getCar(Long.valueOf(params.get("car_id")));
        //clientDTO = clientDAO.getClient(carDTO.getClient().getId());
        //String temp = params.get("client_id");
        //clientDTO = clientDAO.getClient(Long.valueOf(params.get("customer_id")));      
    }
    */    
        
        
    
    public List<CarDTO> showCars() {
        return carDAO.getCars();
    }

    public List<ClientDTO> showClients() {
        return clientDAO.showClients();
    }
    
    public void sellCar(CarDTO carDTO) {
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction transaction = session.beginTransaction();
        
        String hql = "FROM Car car WHERE car.id = :carId";
        List<Car> cars = session.createQuery(hql)
                .setParameter("carId", carDTO.getId())
                .list();
        Car car = cars.get(0);
        car.setStatus(Car.Status.SOLD);   
                
        transaction.commit();
    }
    
  
    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }


    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public CarDTO getCarDTO() {
        return carDTO;
    }

    public void setCarDTO(CarDTO carDTO) {
        this.carDTO = carDTO;
    }

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }
    
    public void showClient() throws IOException {
        ClientDAO clientDAO = new ClientDAO();
        this.clientDTO = clientDAO.getClient(clientDTO.getId());
        

    }
}