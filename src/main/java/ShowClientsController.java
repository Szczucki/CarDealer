/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.cardealer.model.CarDAO;
import com.mycompany.cardealer.model.CarDTO;
import com.mycompany.cardealer.model.ClientDAO;
import com.mycompany.cardealer.model.ClientDTO;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;



/**
 *
 * @author verit
 */
@ManagedBean(name = "showClientsController")
@ViewScoped
public class ShowClientsController implements Serializable{

    private CarDTO carDTO = new CarDTO();
    private CarDAO carDAO = new CarDAO();
    
    private ClientDAO clientDAO = new ClientDAO();

    public String goToPageWithObject (Object object, String objectName, String url) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash()
                .put(objectName, object);
        return url;
    }
    public CarDTO getCarDTO() {
        return carDTO;
    }

    public void setCarDTO(CarDTO carDTO) {
        this.carDTO = carDTO;
    }
    
    public ShowClientsController() {
    }
    
    public List<CarDTO> showCars () {
    return carDAO.getCars();
    }
    
    public List<ClientDTO> showClients() {
        return clientDAO.showClients();
    }
    /*
    public List<Client> showClientsNoDTO() {
        List<Client> clients = new ArrayList<Client>();

        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction beginTransaction = session.beginTransaction();
        Query query = session.createQuery("FROM Client");
        clients = query.list();
        beginTransaction.commit();
        return clients;
    }
    */
}
