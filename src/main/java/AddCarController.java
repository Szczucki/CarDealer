/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.cardealer.model.CarDAO;
import com.mycompany.cardealer.model.CarDTO;
import com.mycompany.cardealer.model.CarTransaction.TransactionType;
import com.mycompany.cardealer.model.CarTransactionDAO;
import com.mycompany.cardealer.model.Client;
import com.mycompany.cardealer.model.ClientDTO;
import com.mycompany.cardealer.model.CarTransactionDTO;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "addCarController")
@RequestScoped
public class AddCarController {

    /**
     * Creates a new instance of GuestBookController
     */
    private CarDTO carDTO = new CarDTO();
    private ClientDTO clientDTO = new ClientDTO();
    private Client client = new Client();
    private CarTransactionDTO carTransactionDTO = new CarTransactionDTO();
    private CarDAO carDAO = new CarDAO();

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }

    public CarDTO getCarDTO() {
        return carDTO;
    }

    public void setCarDTO(CarDTO carDTO) {
        this.carDTO = carDTO;
    }

    public void addCar(CarDTO carDTO) {
        
        CarTransactionDAO carTransactionDAO = new CarTransactionDAO(); 
        carTransactionDAO.addCarTransaction((carDAO.addCar(carDTO)),TransactionType.BUY);

    } 
}