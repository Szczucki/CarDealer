/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.cardealer.model.ClientDAO;
import com.mycompany.cardealer.model.ClientDTO;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author verit
 */
@ManagedBean(name = "addClientController")
@RequestScoped
public class AddClientController implements Serializable{

    private ClientDTO clientDTO = new ClientDTO();

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }
    
    public AddClientController() {
    }
    
    public void addClient(ClientDTO clientDTO) {
        ClientDAO clientDAO = new ClientDAO();
        clientDAO.addClient(clientDTO);

    }
    
}
