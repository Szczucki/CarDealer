/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.mycompany.cardealer.model.Car;
import com.mycompany.cardealer.model.CarDAO;
import com.mycompany.cardealer.model.CarDTO;
import com.mycompany.cardealer.model.ClientDAO;
import com.mycompany.cardealer.model.ClientDTO;
import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;


/**
 *
 * @author RENT
 */
@ManagedBean(name = "carDetailsController")
@ViewScoped
public class CarDetailsController implements Serializable {

    Map<String, String> params;
    Long carId;
    Car car;
    CarDTO carDTO = (CarDTO) getObjectFromFlash("carDTO");
    CarDAO carDAO = new CarDAO();
    
    //@ManagedProperty("#{flash}")
    //private Flash flash;
    
    public Object getObjectFromFlash (String objectName){
        Object object = FacesContext.getCurrentInstance().getExternalContext()
                .getFlash().get(objectName);
       FacesContext.getCurrentInstance().getExternalContext()
                .getFlash().keep(objectName);
        //flash.keep(objectName);
        return object;
    }
        
    public String goToPageWithObject(Object object, String objectName, String url) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash()
                .put(objectName, object);
        return url;
    }
    /*@PostConstruct
    public void init(){
        params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        String temp = params.get("car_id");
        carDTO = carDAO.getCar(Long.valueOf(temp));
              
    }
    
    CarDetailsController {
    this.carDTO = (CarDTO) FacesContext.getCurrentInstance().getExternalContext()
                    .getFlash().get("flashCar");
    }*/
    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public CarDTO getCarDTOFromFlash() {
        /**
         * Takes the parameter from the flash context
         */
        return (CarDTO) FacesContext.getCurrentInstance().getExternalContext()
                .getFlash().get("flashCar");
    }
   /* public Long getCarId() {
        params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        String temp = params.get("car_id");
        carId = Long.valueOf(temp);
        return carId;
    }*/

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public CarDTO getCarDTO() {
        return carDTO;
    }

    public void setCarDTO(CarDTO carDTO) {
        this.carDTO = carDTO;
    }
    
  
    
}