/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardealer.model;

import com.mycompany.cardealer.ConfigHibernate;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author verit
 */
public class CarDAO {
    
    
    public Car addCar(CarDTO carDTO) {
        Car car = new Car();
        car.setBrand(carDTO.getBrand());
        car.setModel(carDTO.getModel());
        car.setVin(carDTO.getVin());
        car.setProductionYear(carDTO.getProductionYear());
        car.setOcNumber(carDTO.getOcNumber());
        car.setRegNumber(carDTO.getRegNumber());
        car.setFuel(carDTO.getFuel());
        car.setStatus(carDTO.getStatus());
        car.setGearbox(carDTO.getGearbox());
        car.setMileage(carDTO.getMileage());
        car.setEngineCC(carDTO.getEngineCC());
        car.setPower(carDTO.getPower());
        car.setPrice(carDTO.getPrice());
        car.setDescription(carDTO.getDescription());
        car.setTests(0);

        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction beginTransaction = session.beginTransaction();

        String hql = "FROM Client client WHERE client.id = :clientId";
        List<Client> clients = session.createQuery(hql)
                .setParameter("clientId", carDTO.getClientId())
                .list();

        car.setClient(clients.get(0));

        session.save(car);
        beginTransaction.commit();
        return car;
    }
    
    public List<CarDTO> getCars() {
        List<CarDTO> carDTOs = new ArrayList<CarDTO>();

        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction beginTransaction = session.beginTransaction();
        Query query = session.createQuery("FROM Car");
        List<Car> cars = query.list();
        
        for (int i = 0; i < cars.size(); i++) {
            carDTOs.add(new CarDTO());
            carDTOs.get(i).setId(cars.get(i).getId());
            carDTOs.get(i).setBrand(cars.get(i).getBrand());
            carDTOs.get(i).setModel(cars.get(i).getModel());
            carDTOs.get(i).setVin(cars.get(i).getVin());
            carDTOs.get(i).setRegNumber(cars.get(i).getRegNumber());
            carDTOs.get(i).setOcNumber(cars.get(i).getOcNumber());
            carDTOs.get(i).setProductionYear(cars.get(i).getProductionYear());
            carDTOs.get(i).setMileage(cars.get(i).getMileage());
            carDTOs.get(i).setEngineCC(cars.get(i).getEngineCC());
            carDTOs.get(i).setFuel(cars.get(i).getFuel());
            carDTOs.get(i).setGearbox(cars.get(i).getGearbox());
            carDTOs.get(i).setPrice(cars.get(i).getPrice());
            carDTOs.get(i).setStatus(cars.get(i).getStatus());
            carDTOs.get(i).setDescription(cars.get(i).getDescription());
        }
        beginTransaction.commit();
        return carDTOs;
    }
    
    public CarDTO getCar(Long carId) {
    
    CarDTO carDTO = new CarDTO();
    SessionFactory instance = ConfigHibernate.getInstance();
    Session session = instance.openSession();
    Transaction transaction = session.beginTransaction();

    String hql = "FROM Car car WHERE car.id = :carId";
    List<Car> cars = session.createQuery(hql)
            .setParameter("carId", carId)
            .list();
    transaction.commit ();
    
        carDTO.setBrand(cars.get(0).getBrand());
        carDTO.setModel(cars.get(0).getModel());
        carDTO.setVin(cars.get(0).getVin());
        carDTO.setRegNumber(cars.get(0).getRegNumber());
        carDTO.setOcNumber(cars.get(0).getOcNumber());
        carDTO.setProductionYear(cars.get(0).getProductionYear());
        carDTO.setMileage(cars.get(0).getMileage());
        carDTO.setEngineCC(cars.get(0).getEngineCC());
        carDTO.setFuel(cars.get(0).getFuel());
        carDTO.setGearbox(cars.get(0).getGearbox());
        carDTO.setPrice(cars.get(0).getPrice());
        carDTO.setStatus(cars.get(0).getStatus());
        carDTO.setDescription(cars.get(0).getDescription());
        
    return carDTO;
    
    }
}
