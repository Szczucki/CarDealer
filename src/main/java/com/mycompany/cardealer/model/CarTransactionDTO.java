/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardealer.model;


import java.io.Serializable;


/**
 *
 * @author Msi
 */

public class CarTransactionDTO implements Serializable {

    public enum TransactionType {
    SELL, BUY;
}
    private static final long serialVersionUID = 1L;
    private Long id;
    private TransactionType type;
    private Car car;
    private Client client;
    private Contract contract;
    private CarTransaction carTransaction;

    public CarTransaction getCarTransaction() {
        return carTransaction;
    }

    public void setCarTransaction(CarTransaction carTransaction) {
        this.carTransaction = carTransaction;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Car getCar() {
        return car;
    }

    public Client getClient() {
        return client;
    }

    public Contract getContract() {
        return contract;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

   
    
}
