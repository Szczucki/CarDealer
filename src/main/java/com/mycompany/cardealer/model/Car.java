/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardealer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Msi
 */

@Entity
@Table(name = "cars")
public class Car implements Serializable {
    
    public enum Fuel {
        DIESEL, PETROL, LPG, HYBRID, ELECTRIC;        
    }
    
    public enum Gearbox {
        MANUAL, AUTO, HYBRID, CVT;
    }

    public enum Status {
        RENT, //rent from Client to Car Dealer for sale
        BOUGHT, //bought from Client to Car dealer for sale
        SOLD //sold from Client to Client (if RENT) or from Car Dealer to Client
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String vin;
    @Column
    private Integer productionYear;
    @Column
    private String brand;
    @Column
    private String model;
    @Column
    private String ocNumber;
    @Column
    private String regNumber;
    @Column
    @Enumerated (STRING)
    private Fuel fuel;
    @Column
    private Long mileage;
    @Column
    private String engineCC;
    @Column
    private Integer power;
    @Column
    private Gearbox gearbox;
    @Column
    private String description;
    @Column
    private Integer tests;
    @Column
    private Status status;
    @Column
    private BigDecimal price;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id")
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    
    public void setVin(String vin) {
        this.vin = vin;
    }

    public void setProductionYear(Integer productionYear) {
        this.productionYear = productionYear;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setOcNumber(String ocNumber) {
        this.ocNumber = ocNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

    public void setEngineCC(String engineCC) {
        this.engineCC = engineCC;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public void setGearbox(Gearbox gearbox) {
        this.gearbox = gearbox;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTests(Integer tests) {
        this.tests = tests;
    }

    public String getVin() {
        return vin;
    }

    public Integer getProductionYear() {
        return productionYear;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getOcNumber() {
        return ocNumber;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public Fuel getFuel() {
        return fuel;
 
    }

    public Long getMileage() {
        return mileage;
    }

    public String getEngineCC() {
        return engineCC;
    }

    public Integer getPower() {
        return power;
    }

    public Gearbox getGearbox() {
        return gearbox;
    }

    public String getDescription() {
        return description;
    }

    public Integer getTests() {
        return tests;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    

    
}
