/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardealer.model;

import com.mycompany.cardealer.ConfigHibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author verit
 */
public class CarTransactionDAO {
    
    public void addCarTransaction(Car car, CarTransaction.TransactionType transactionType) {
        CarTransaction carTransaction = new CarTransaction();
        carTransaction.setCar(car);
        carTransaction.setClient(car.getClient());
        carTransaction.setType(transactionType);

        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(carTransaction);
        transaction.commit();

    }
}

