/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardealer.model;

import com.mycompany.cardealer.ConfigHibernate;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author verit
 */
public class ClientDAO {
    public void addClient(ClientDTO clientDTO) {
        Client client = new Client();
        client.setName(clientDTO.getName());
        client.setSurname(clientDTO.getSurname());
        client.setAddress(clientDTO.getAddress());
        client.setNip(clientDTO.getNip());
        client.setPesel(clientDTO.getPesel());
        client.setPhone(clientDTO.getPhone());
        SessionFactory instance = ConfigHibernate.getInstance();
        Session openSession = instance.openSession();
        Transaction beginTransaction = openSession.beginTransaction();
        openSession.save(client);
        beginTransaction.commit();
    }
    
    public List<ClientDTO> showClients() {
        List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();

        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction beginTransaction = session.beginTransaction();
        Query query = session.createQuery("FROM Client");
        List<Client> clients = query.list();
        
        for (int i = 0; i < clients.size(); i++) {
            clientDTOs.add(new ClientDTO());
            clientDTOs.get(i).setId(clients.get(i).getId());
            clientDTOs.get(i).setName(clients.get(i).getName());
            clientDTOs.get(i).setAddress(clients.get(i).getAddress());
            clientDTOs.get(i).setPesel(clients.get(i).getPesel());
            clientDTOs.get(i).setNip(clients.get(i).getNip());
            clientDTOs.get(i).setPhone(clients.get(i).getPhone());
        }
        beginTransaction.commit();
        return clientDTOs;
    }
    
    public ClientDTO getClient(Long clientId) {
        ClientDTO clientDTO = new ClientDTO();
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction transaction = session.beginTransaction();
        String hql = "FROM Client client WHERE client.id = :clientId";
        List<Client> clients = session.createQuery(hql)
                .setParameter("clientId", clientId)
                .list();

        transaction.commit();

        clientDTO.setName(clients.get(0).getName());
        clientDTO.setSurname(clients.get(0).getSurname());
        clientDTO.setAddress(clients.get(0).getAddress());
        clientDTO.setNip(clients.get(0).getNip());
        clientDTO.setPesel(clients.get(0).getPesel());
        clientDTO.setPhone(clients.get(0).getPhone());
        clientDTO.setId(clients.get(0).getId());
        
        return clientDTO;
    }
}
