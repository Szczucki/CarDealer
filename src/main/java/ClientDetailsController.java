/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.mycompany.cardealer.model.Car;
import com.mycompany.cardealer.model.CarDAO;
import com.mycompany.cardealer.model.CarDTO;
import com.mycompany.cardealer.model.Client;
import com.mycompany.cardealer.model.ClientDAO;
import com.mycompany.cardealer.model.ClientDTO;
import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;


/**
 *
 * @author RENT
 */
@ManagedBean(name = "clientDetailsController")
@ViewScoped
public class ClientDetailsController implements Serializable{

    Map<String, String> params;
    Long clientId;
    Client client; 
    ClientDTO clientDTO = (ClientDTO)FacesContext.getCurrentInstance().getExternalContext()
            .getFlash().get("clientDTO");
    
    ClientDAO clientDAO = new ClientDAO();
    
    public String goToPageWithObject(Object object, String objectName, String url) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash()
                .put(objectName, object);
        return url;
    }
 
    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

   /* public Long getCarId() {
        params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        String temp = params.get("car_id");
        carId = Long.valueOf(temp);
        return carId;
    }*/

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }
    
   
}