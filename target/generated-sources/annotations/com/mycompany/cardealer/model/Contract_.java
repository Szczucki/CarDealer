package com.mycompany.cardealer.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Contract.class)
public abstract class Contract_ {

	public static volatile ListAttribute<Contract, CarTransaction> carTransactions;
	public static volatile SingularAttribute<Contract, Long> id;
	public static volatile SingularAttribute<Contract, String> text;
	public static volatile SingularAttribute<Contract, String> type;

}

