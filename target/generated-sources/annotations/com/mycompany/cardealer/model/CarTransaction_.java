package com.mycompany.cardealer.model;

import com.mycompany.cardealer.model.CarTransaction.TransactionType;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CarTransaction.class)
public abstract class CarTransaction_ {

	public static volatile SingularAttribute<CarTransaction, Car> car;
	public static volatile SingularAttribute<CarTransaction, Contract> contract;
	public static volatile SingularAttribute<CarTransaction, Client> client;
	public static volatile SingularAttribute<CarTransaction, Long> id;
	public static volatile SingularAttribute<CarTransaction, TransactionType> type;

}

