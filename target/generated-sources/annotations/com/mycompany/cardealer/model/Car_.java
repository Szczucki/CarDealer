package com.mycompany.cardealer.model;

import com.mycompany.cardealer.model.Car.Fuel;
import com.mycompany.cardealer.model.Car.Gearbox;
import com.mycompany.cardealer.model.Car.Status;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Car.class)
public abstract class Car_ {

	public static volatile SingularAttribute<Car, String> engineCC;
	public static volatile SingularAttribute<Car, Fuel> fuel;
	public static volatile SingularAttribute<Car, String> description;
	public static volatile SingularAttribute<Car, Integer> productionYear;
	public static volatile SingularAttribute<Car, String> regNumber;
	public static volatile SingularAttribute<Car, Integer> tests;
	public static volatile SingularAttribute<Car, String> ocNumber;
	public static volatile SingularAttribute<Car, BigDecimal> price;
	public static volatile SingularAttribute<Car, Client> client;
	public static volatile SingularAttribute<Car, String> vin;
	public static volatile SingularAttribute<Car, String> model;
	public static volatile SingularAttribute<Car, Long> id;
	public static volatile SingularAttribute<Car, Integer> power;
	public static volatile SingularAttribute<Car, Gearbox> gearbox;
	public static volatile SingularAttribute<Car, String> brand;
	public static volatile SingularAttribute<Car, Long> mileage;
	public static volatile SingularAttribute<Car, Status> status;

}

