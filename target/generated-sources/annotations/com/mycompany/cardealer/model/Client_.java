package com.mycompany.cardealer.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Client.class)
public abstract class Client_ {

	public static volatile ListAttribute<Client, Car> cars;
	public static volatile SingularAttribute<Client, String> address;
	public static volatile SingularAttribute<Client, String> nip;
	public static volatile SingularAttribute<Client, String> phone;
	public static volatile SingularAttribute<Client, String> surname;
	public static volatile SingularAttribute<Client, String> name;
	public static volatile SingularAttribute<Client, Long> id;
	public static volatile SingularAttribute<Client, String> pesel;
	public static volatile ListAttribute<Client, CarTransaction> transactions;

}

