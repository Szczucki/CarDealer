package com.mycompany.cardealer.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Purchase.class)
public abstract class Purchase_ {

	public static volatile SingularAttribute<Purchase, Car> car;
	public static volatile SingularAttribute<Purchase, Contract> contract;
	public static volatile SingularAttribute<Purchase, Client> client;
	public static volatile SingularAttribute<Purchase, Long> id;

}

